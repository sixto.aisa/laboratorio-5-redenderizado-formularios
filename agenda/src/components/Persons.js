const  Persons=(props) =>{

    return (
        <ul>
            {
                props.data.map((person, index) => {
                    return (
                        <li key={person.name}>{person.name}  {person.number}</li>
                    );

                })
            }
        </ul>
    )
}

export default Persons
