const  PersonForm=(props) =>{

    return (
        <div>
            <form onSubmit={props.handleSubmit}>
                <div>
                    name: <input type="text" onChange={props.handleChangeName} value={props.name} />
                </div>
                <div>
                    number: <input type="number" onChange={props.handleChangeNumber} value={props.number} />
                </div>
                <div>
                    <button >add</button>
                </div>
            </form>
        </div>
    )
}

export default PersonForm
