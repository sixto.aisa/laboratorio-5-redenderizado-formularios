import ReactDOM from "react-dom";
import React, {useState} from 'react'
import PersonForm from "./components/PersonForm";
import Persons from "./components/Persons";
import Filter from "./components/Filter";

const App = () => {
    const [persons, setPersons] = useState([
        {
            name: 'Arto Hellas',
            number: '051-99999999'
        }
    ])
    const [newName, setNewName] = useState('')
    const [newNumber, setNewNumber] = useState('')
    const [search, setSearch] = useState('')

    const handleSubmit = (event) => {
        event.preventDefault()

        const nuevaPersona = {
            name: newName,
            number: newNumber
        }

        let duplicados = persons.filter((person) => {
            return person.name === newName
        })

        console.log(duplicados)

        if (duplicados.length > 0) {
            alert(`${nuevaPersona.name} ya existe `)
            return
        }

        setPersons([
            ...persons, nuevaPersona,
        ])

        setNewName('')
        setNewNumber('')

    }

    const handleChangeName = (event) => {
        setNewName(event.target.value)
    }

    const handleChangeNumber = (event) => {
        setNewNumber(event.target.value)
    }

    let filtroPersonas = persons

    if (search.length > 0) {
        filtroPersonas = persons.filter((person) => {
            return person.name.toUpperCase().includes(search)
        })
    }

    const handleChangeSearch = (event) => {
        setSearch(event.target.value.toUpperCase())
    }

    return (

        <div>
            <h2>Phonebook</h2>
            <Filter search={search}
                    handleChangeSearch={handleChangeSearch}  />

            <hr/>
            <h2>add a new</h2>
            <PersonForm name={newName} number={newNumber}
                        handleChangeName={handleChangeName}
                        handleChangeNumber={handleChangeNumber}
                        handleSubmit={handleSubmit}/>

            <hr/>
            <h2>Numbers</h2>
            <Persons data={filtroPersonas}/>


        </div>
    )
}

ReactDOM.render(<App/>, document.getElementById('root'))
