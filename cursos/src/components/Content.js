import Part from "./Part";

const  Content=(props) =>{


    const reducer =(accumulator,p) => accumulator + p.exercises
    const total=props.parts.reduce(reducer,0)

    return (
        <div>
            {
                props.parts.map((part, index) => {
                    return (
                        <Part key={part.id} nombre={part.name} numero={part.exercises} ></Part>
                    );
                })
            }

            <p>Total {total}</p>


        </div>
    )
}

export default Content
